FROM node:current-alpine3.12
WORKDIR /usr/src/app
COPY package*.json ./
#build
RUN npm install
COPY . .
#test
RUN npm run test
EXPOSE 3000
#pasar a usuario node para ejecutar la app
USER node
CMD [ "node", "index.js" ]
